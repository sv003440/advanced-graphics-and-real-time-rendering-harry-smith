#pragma once
//based on the FullScreenRenderTarget class in Real-Time 
//Rendering with DirectX and HLSL by Paul Varcholik
#include <d3d11_1.h>
#include <directxmath.h>

using namespace DirectX;

struct vertex_deferred
{
	XMFLOAT3 Position;
	XMFLOAT2 TextureCoord;
};

class FullScreenRenderTarget
{
private:
	ID3D11Texture2D* FullScreenTexture = nullptr;

	ID3D11RenderTargetView* RenderTargetView = nullptr;
	ID3D11ShaderResourceView* ShaderResourceView = nullptr;
	ID3D11DepthStencilView* DepthStencilView = nullptr;

	ID3D11Device* Device;
	ID3D11DeviceContext* ImmediateContext;

	ID3D11InputLayout* Layout = nullptr;
	ID3D11VertexShader* VertexShader = nullptr;
	ID3D11PixelShader* PixelShader = nullptr;

	ID3D11Buffer*  VertexBuffer = nullptr;
	ID3D11Buffer*  IndexBuffer = nullptr;

public:
	FullScreenRenderTarget( ID3D11Device* InDevice,  ID3D11DeviceContext* InImmediateContext, UINT InWidth, UINT InHeight);
	~FullScreenRenderTarget();

	ID3D11RenderTargetView* GetRenderTargetView() const { return RenderTargetView; };
	ID3D11ShaderResourceView* GetShaderResourceView() const { return ShaderResourceView;};
	ID3D11DepthStencilView* GetDepthStencilView() const { return DepthStencilView; };

	HRESULT InitIndexBuffer();
	HRESULT InitVertexBuffer();

	HRESULT InitShadersAndInputLayout(ID3DBlob* VS_Blob, ID3DBlob* PS_Blob);

	HRESULT InitShaders(ID3DBlob* VS_Blob, ID3DBlob* PS_Blob);
	HRESULT InitInputLayout(ID3DBlob* VS_Blob);

	void RenderFromTexture(ID3D11SamplerState* Sampler);

	void SetOMRenderTarget();
};
