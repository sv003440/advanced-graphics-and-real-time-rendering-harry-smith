#pragma once

#include <windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include "DDSTextureLoader.h"
#include "resource.h"
#include "Camera.h"

#include "FullScreenRenderTarget.h"

#include <vector>

#include "GameObject.h"
#include "Lights.h"

#include "ShadowMapper.h"
#include"G_Buffer.h"
#include "MathHelper.h"

using namespace DirectX;

struct vertex_simple
{
    XMFLOAT3 Position_Local;
	XMFLOAT3 Normal_Local;
	XMFLOAT2 TextureCoord;
	XMFLOAT3 Tangent_Local;
};

//struct g_buffer
//{
//	XMFLOAT4 Normal;
//	XMFLOAT4 DiffuseAlbedo;
//	XMFLOAT4 SpecularAlbedo;
//	XMFLOAT4 Position;
//};


struct constant_buffer
{
	XMMATRIX World;
	XMMATRIX View;
	XMMATRIX Projection;
	
	Material Material;

	DirectionalLight Light;

	XMFLOAT3 EyePosW;

	float HasTexture;
};





class Application
{
private:
	G_Buffer* GBuffer = nullptr;
	ID3D11PixelShader*      PixelShader_DeferredPrePass;
	ID3D11PixelShader*      PixelShader_DeferredLightPass;


private:
	ShadowMapper* ShadowMap;


	ID3D11Buffer* ConstantBuffer_Shadows;

	static const int ShadowMapSize = 2048;
	BoundingSphere* SceneBounds = nullptr; 

	ID3DBlob* VS_SHADOWS_Blob = nullptr;
	ID3DBlob* PS_SHADOWS_Blob = nullptr;


private:

	FullScreenRenderTarget* DeferredRenderTarget = nullptr;

private:
	HINSTANCE               _hInst;
	HWND                    _hWnd;
	D3D_DRIVER_TYPE         _driverType;
	D3D_FEATURE_LEVEL       _featureLevel;

	ID3D11Device*           Device;
	ID3D11DeviceContext*    ImmediateContext;
	IDXGISwapChain*         SwapChain;
	ID3D11RenderTargetView* BackBufferRTV;

	ID3D11VertexShader*     VertexShader;
	ID3D11PixelShader*      PixelShader;

	ID3D11InputLayout*      VertexLayout;

	ID3D11Buffer*           VertexBuffer_Box;
	ID3D11Buffer*           IndexBuffer_Box;

	ID3D11Buffer*           VertexBuffer_Plane;
	ID3D11Buffer*           IndexBuffer_Plane;

	ID3D11Buffer*           ConstantBuffer;

	ID3D11DepthStencilView* DepthStencilView = nullptr;
	ID3D11Texture2D* DepthStencilBuffer = nullptr;

	ID3D11ShaderResourceView * TextureRV_Box = nullptr;

	ID3D11ShaderResourceView* Color_Box = nullptr;
	ID3D11ShaderResourceView* Displacement_Box = nullptr;
	ID3D11ShaderResourceView* Normals_Box = nullptr;
	ID3D11ShaderResourceView* Occlusion_Box = nullptr;

	ID3D11ShaderResourceView* Stones_Diffuse = nullptr;
	ID3D11ShaderResourceView* Stones_Normal = nullptr;

	ID3D11ShaderResourceView* Bricks_Diffuse = nullptr;
	ID3D11ShaderResourceView* Bricks_Normal = nullptr;

	ID3D11ShaderResourceView* RandomNormals = nullptr;


	ID3D11ShaderResourceView * TextureRV_Floor = nullptr;

	ID3D11SamplerState * LinearSampler = nullptr;

	DirectionalLight DirLight;

	vector<GameObject *> GameObjects;

	//TODO: Place these in camera class?
	Camera * MainCamera;
	float _cameraOrbitRadius = 7.0f;
	float _cameraOrbitRadiusMin = 2.0f;
	float _cameraOrbitRadiusMax = 50.0f;
	float _cameraOrbitAngleXZ = -90.0f;
	float _cameraSpeed = 2.0f;

	UINT WindowHeight;
	UINT WindowWidth;

	// Render dimensions - Change here to alter screen resolution
	UINT RenderHeight = 1080;
	UINT RenderWidth = 1920;

	ID3D11DepthStencilState* DSLessEqual;
	ID3D11RasterizerState* RSCullNone;

	ID3D11RasterizerState* CCWcullMode;
	ID3D11RasterizerState* CWcullMode;

private:
	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
	HRESULT InitDevice();
	void Cleanup();
	HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
	HRESULT InitShadersAndInputLayout();
	HRESULT InitVertexBuffer();
	HRESULT InitIndexBuffer();

	void MoveForward(int ObjectNumber);

	void DrawSceneObjects(constant_buffer& InOutCB);

public:
	Application();
	~Application();

	HRESULT Initialise(HINSTANCE hInstance, int nCmdShow);

	bool HandleKeyboard(MSG InMessage);

	void Update();
	void Draw();

	//Mouse camera control functions from Frank Luna
	void OnMouseDown(WPARAM State, int x, int y);
	void OnMouseUp(WPARAM State, int x, int y);
	void OnMouseMove(WPARAM State, int x, int y);

	LRESULT MessageProcedure(HWND HWnd, UINT Message, WPARAM WParam, LPARAM LParam);


private:
	POINT LastMousePosition;


	float Radius = 2.5;

	// Controls:
	//		Hold the left mouse button down and move the mouse to rotate.
	//      Hold the right mouse button down to zoom in and out.
};

