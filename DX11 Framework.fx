//--------------------------------------------------------------------------------------
// File: DX11 Framework.fx
//--------------------------------------------------------------------------------------
//#define PARRALAX 
#define LUNAPARRALAX

//#define GRAYSCALE
//#define COLOURINVERSE
#define SHADOW_MAP

#include "Lights.fx"

Texture2D txDiffuse : register(t0);
Texture2D txHeight : register(t1);
Texture2D txNormal : register(t2);

Texture2D txShadow : register(t3);

Texture2D Normals_Def : register(t4);
Texture2D Diffuse_Def : register(t5);
Texture2D Specular_Def : register(t6);
Texture2D Position_Def : register(t7);
Texture2D RandomNormals : register(t8);


Texture2D txDeferred : register(t10);

SamplerState samLinear : register(s0);

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------

cbuffer constant_buffer : register( b0 )
{
	matrix World;
	matrix View;
	matrix Projection;

	Material surface;
	DirectionalLight light;

	float3 EyePosW;
	float HasTexture;
};


struct VS_RENDERTOTEX_IN
{
	float4 Position : POSITION;
	float2 TexCoords : TEXCOORD0;
};

struct VS_RENDERTOTEX_OUT
{
	float4 Position : SV_POSITION;
	float2 TexCoords : TEXCOORD0;
};

struct VS_INPUT
{
	float3 PosL : POSITION;
	float3 NormL : NORMAL;
	float2 Tex : TEXCOORD0;
	float3 Tangent : TANGENT;
};

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 PosH : SV_POSITION;
	float3 NormW : NORMAL;

	float3 PosW : POSITION;
	float2 Tex : TEXCOORD0;

	float3 Tangent : TANGENT;
};


struct G_BUFFER // used for 'real' deferred rendering where multiple data are rendered to multiple RTs
{
	float4 Normal : SV_Target0;
	float4 DiffuseAlbedo : SV_Target1;
	float4 SpecularAlbedo : SV_Target2;
	float4 Position : SV_Target3;
};

float3 NormalSampleToWorldSpace(float3 NormalMapSample, float3 Normal_World, float3 Tangent_World)
{
	float3 Normal_Tangent = 2.0f * NormalMapSample - 1.0f;

	float3 N = Normal_World;
	float3 T = normalize(Tangent_World - dot(Tangent_World, N)*N);
	// ||N|| == 1, projN(T) == dot(T,N)*N.
	// Therefore T - projN(T) is the portion of T orthogonal to N
	// So sayeth Frank Luna

	float3 B = cross(N, T);

	float3x3 TBN = float3x3(T, B, N);

	float3 BumpNormal_World = mul(Normal_Tangent, TBN);

	return BumpNormal_World;
}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT)0;

	float4 posW = mul(float4(input.PosL, 1.0f), World);
	output.PosW = posW.xyz;

	output.PosH = mul(posW, View);
	output.PosH = mul(output.PosH, Projection);
	output.Tex = input.Tex;

	output.NormW = normalize(mul(float4(input.NormL, 0.0f), World).xyz);
	output.Tangent = normalize(mul(float4(input.Tangent, 0.0f), World).xyz);

    return output;
}



float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 toEye = normalize(EyePosW - input.PosW);
	float3 FromEye = -toEye;

	float3 Normal = normalize(input.NormW);
	float2 FinalCoords = input.Tex;

#ifdef PARRALAX
	float HeightScale = 0.02f;
	
	float HeightBias = -0.02f;
	float BiasedHeight = txHeight.Sample(samLinear, input.Tex) * HeightScale + HeightBias;
	
	FinalCoords = input.Tex + (toEye.xy * BiasedHeight);
#endif


#ifdef LUNAPARRALAX
	//ortho basis
	float3 Tangent = normalize(input.Tangent - dot(input.Tangent, Normal) * Normal);
	float3 Bitangent = cross(Normal, Tangent);

	float HeightMapScale = 0.04f;
	float MinSamples = 4;
	float MaxSamples = 40;

	float3x3 ToTangent = transpose(float3x3(Tangent, Bitangent, Normal));
	float3 ViewDirection_Tangent = mul(FromEye, ToTangent);

	float2 MaxParallaxOffset = (-ViewDirection_Tangent.xy * HeightMapScale) / ViewDirection_Tangent.z;

	int SampleCount = (int)lerp(MaxSamples, MinSamples, dot(toEye, Normal));

	float Step = 1.0f / (float)SampleCount;

	float2 TexStep = MaxParallaxOffset * Step;

	float2 dx = ddx(input.Tex);
	float2 dy = ddy(input.Tex);

	int SampleIndex = 0;
	float2 CurrentTexOffset = 0;
	float2 PreviousTexOffset = 0;
	float2 FinalTexOffset = 0;
	float CurrentRayZ = 1.0f - Step;
	float PreviousRayZ = 1.0f;
	float CurrentHeight = 0.0f;
	float PreviousHeight = 0.0f;

	while (SampleIndex < SampleCount + 1)
	{
		CurrentHeight = txNormal.SampleGrad(samLinear, input.Tex + CurrentTexOffset, dx, dy).a;


		if (CurrentHeight > CurrentRayZ)
		{
			float t = (PreviousHeight - PreviousRayZ) / (PreviousHeight - CurrentHeight + CurrentRayZ - PreviousRayZ);
			FinalTexOffset = PreviousTexOffset + t *TexStep;

			SampleIndex = SampleCount + 1;
		}
		else
		{
			++SampleIndex;

			PreviousTexOffset = CurrentTexOffset;
			PreviousRayZ = CurrentRayZ;
			PreviousHeight = CurrentHeight;

			CurrentTexOffset += TexStep;

			CurrentRayZ -= Step;
		}
	}

	FinalCoords = input.Tex + FinalTexOffset;
#endif

	float3 NormalSample = txNormal.Sample(samLinear, FinalCoords).rgb;
	float3 FinalNormal = NormalSampleToWorldSpace(NormalSample, Normal, input.Tangent);


	// Get texture data from file
	//float4 textureColour = txDiffuse.Sample(samLinear, input.Tex);
	float4 textureColour = txDiffuse.Sample(samLinear, FinalCoords);


	float4 ambient;// = float3(0.0f, 0.0f, 0.0f);
	float4 diffuse;// = float3(0.0f, 0.0f, 0.0f);
	float4 specular;// = float3(0.0f, 0.0f, 0.0f);

	ComputeDirectionalLight(surface, light, FinalNormal.xyz, toEye, ambient, diffuse, specular);

	// Sum all the terms together and copy over the diffuse alpha.
	float4 finalColour;

	if (HasTexture == 1.0f)
	{
		finalColour.rgb = (textureColour.rgb * (ambient + diffuse)) + specular;
	}
	else
	{
		finalColour.rgb = ambient + diffuse + specular;
	}

	finalColour.a = surface.Diffuse.a;

	return finalColour;
}


//Screen Space FX
VS_RENDERTOTEX_OUT VS_RENDERTOTEX(VS_RENDERTOTEX_IN VS_Input)
{
	VS_RENDERTOTEX_OUT VS_Output = (VS_RENDERTOTEX_OUT)0;

	VS_Output.Position = VS_Input.Position;
	VS_Output.TexCoords = VS_Input.TexCoords;

	return VS_Output;
}


float4 PS_RENDERTOTEX(VS_RENDERTOTEX_OUT PS_input) : SV_Target
{
	float4 Colour = txDeferred.Sample(samLinear, PS_input.TexCoords);

#ifdef GRAYSCALE
	float Intensity = dot(Colour.rgb, float3(0.2f, 0.5f, 0.1f));

	return float4(Intensity.rrr, Colour.a);
#endif
#ifdef COLOURINVERSE
	return float4(1 - Colour.rgb, Colour.a);
#endif


	return txDeferred.Sample(samLinear, PS_input.TexCoords);
}


//Deferred Rendering for reals

G_BUFFER PS_DEFERRED_PRE(VS_OUTPUT input)
{
	float3 toEye = normalize(EyePosW - input.PosW);
	float3 FromEye = -toEye;

	float3 Normal = normalize(input.NormW);
	float2 FinalCoords = input.Tex;

#ifdef PARRALAX
	float HeightScale = 0.02f;

	float HeightBias = -0.02f;
	float BiasedHeight = txHeight.Sample(samLinear, input.Tex) * HeightScale + HeightBias;

	FinalCoords = input.Tex + (toEye.xy * BiasedHeight);
#endif


#ifdef LUNAPARRALAX
	//ortho basis
	float3 Tangent = normalize(input.Tangent - dot(input.Tangent, Normal) * Normal);
	float3 Bitangent = cross(Normal, Tangent);

	float HeightMapScale = 0.04f;
	float MinSamples = 4;
	float MaxSamples = 40;

	float3x3 ToTangent = transpose(float3x3(Tangent, Bitangent, Normal));
	float3 ViewDirection_Tangent = mul(FromEye, ToTangent);

	float2 MaxParallaxOffset = (-ViewDirection_Tangent.xy * HeightMapScale) / ViewDirection_Tangent.z;

	int SampleCount = (int)lerp(MaxSamples, MinSamples, dot(toEye, Normal));

	float Step = 1.0f / (float)SampleCount;

	float2 TexStep = MaxParallaxOffset * Step;

	float2 dx = ddx(input.Tex);
	float2 dy = ddy(input.Tex);

	int SampleIndex = 0;
	float2 CurrentTexOffset = 0;
	float2 PreviousTexOffset = 0;
	float2 FinalTexOffset = 0;
	float CurrentRayZ = 1.0f - Step;
	float PreviousRayZ = 1.0f;
	float CurrentHeight = 0.0f;
	float PreviousHeight = 0.0f;

	while (SampleIndex < SampleCount + 1)
	{
		CurrentHeight = txNormal.SampleGrad(samLinear, input.Tex + CurrentTexOffset, dx, dy).a;


		if (CurrentHeight > CurrentRayZ)
		{
			float t = (PreviousHeight - PreviousRayZ) / (PreviousHeight - CurrentHeight + CurrentRayZ - PreviousRayZ);
			FinalTexOffset = PreviousTexOffset + t *TexStep;

			SampleIndex = SampleCount + 1;
		}
		else
		{
			++SampleIndex;

			PreviousTexOffset = CurrentTexOffset;
			PreviousRayZ = CurrentRayZ;
			PreviousHeight = CurrentHeight;

			CurrentTexOffset += TexStep;

			CurrentRayZ -= Step;
		}
	}

	FinalCoords = input.Tex + FinalTexOffset;
#endif

	float3 NormalSample = txNormal.Sample(samLinear, FinalCoords).rgb;
	float3 FinalNormal = NormalSampleToWorldSpace(NormalSample, Normal, input.Tangent);


	// Get texture data from file
	//float4 textureColour = txDiffuse.Sample(samLinear, input.Tex);
	float4 textureColour = txDiffuse.Sample(samLinear, FinalCoords);


	G_BUFFER Output = (G_BUFFER)0;

	Output.Normal = float4(FinalNormal, 1.0f);
	Output.DiffuseAlbedo = textureColour;// *surface.Diffuse;
	Output.SpecularAlbedo = surface.Specular;
	Output.Position = float4(input.PosW, 1.0f);
	
	return Output;

}


//helper function from Practical Rendering and Computation with D3D11
void GetGBufferAtrributes(in float2 InScreenPos, out float3 OutNormal, out float3 OutPosition, out float3 OutDiffuse, out float3 OutSpecular, out float OutSpecPower)
{
	//sample texture based pn screen position to determine indices
	int3 SampleIndices = int3(InScreenPos.xy, 0);

	OutNormal = Normals_Def.Load(SampleIndices).xyz;
	OutPosition = Position_Def.Load(SampleIndices).xyz;
	OutDiffuse = Diffuse_Def.Load(SampleIndices).xyz;
	float4 Spec = Specular_Def.Load(SampleIndices);
	OutSpecular = Spec.xyz;
	OutSpecPower = Spec.w;
		
}

sampler RandomSampler
{
	AddressU = WRAP;
	AddressV = WRAP;
};
float DoAmbientOcclusion(in float3 InNormal, in float3 InPosition, in float2 InOccluderPosition)
{
	float Scale = 1.0f;
	float Bias = 0.0f;
	float Intensity = 1.0f;

	float3 OccluderPosition = float3(InOccluderPosition, 0.0f);//1.0f?
	float3 ToOccluder = InPosition - OccluderPosition;
	//cache the length, can be scaled if needed
	float DistToOccluder = length(ToOccluder);
	ToOccluder = normalize(ToOccluder);

	return (max(0.0f, dot(InNormal, ToOccluder) - Bias) * (1.0f/(1.0 + DistToOccluder) * Intensity));

}

float CalculateAmbientAccess(in float3 InNormal, in float3 InPosition, in float2 TexCoords)
{
	float2 SurroundingPoints[4] = { 
		float2(1,0),
		float2(-1,0),
		float2(0,1),
		float2(0,-1) };

	//int3 SampleIndex = int3(InPosition.xy, 0);
	float2 RandomNorm = RandomNormals.Sample(RandomSampler, TexCoords).xy;
	float SampleLength = 1.0f;
	float Radius = SampleLength / InPosition.z;

	float AO = 0;
	int Count = 4;

	for (int i = 0; i < Count; ++i)
	{
		float2 Coord1 = reflect(SurroundingPoints[i], RandomNorm) * Radius;
		float2 Coord2 = float2(Coord1.x * 0.707 - Coord1.y*0.707,
								Coord1.x * 0.707 + Coord1.y*0.707);
		
		AO += DoAmbientOcclusion(InNormal, InPosition, TexCoords + Coord1*0.25f);
		AO += DoAmbientOcclusion(InNormal, InPosition, TexCoords + Coord2*0.5f);
		AO += DoAmbientOcclusion(InNormal, InPosition, TexCoords + Coord1*0.75f);
		AO += DoAmbientOcclusion(InNormal, InPosition, TexCoords + Coord2);

	}

	AO /= ((float)Count * 4.0f);

	float Access = (1.0f - AO);

	return saturate(pow(Access, 4.0f));
}

#define AO
float3 CalculateLighting(in float3 InNormal, in float3 InPosition, in float3 InDiffuse, in float3 InSpecular, in float InSpecPower, in float2 TexCoord)
{

	//this one works so so
/*
	float3 LightVec = -light.Direction;
	float3 DiffuseColour = InDiffuse * light.Diffuse * (dot(InNormal, LightVec));

	float3 ToEye = EyePosW - InPosition;
	float3 HTop = LightVec + ToEye;
	float HBot = length(HTop);
	float3 H = HTop / HBot;

	float3 SpecularColour = InSpecular * light.Specular * pow((dot(InNormal, H)), InSpecPower);

	return (DiffuseColour + SpecularColour);
*/
	//this is best
	float3 ToEye = EyePosW - InPosition;
	
	float3 ambient = float3(0.0f, 0.0f, 0.0f);
	float3 diffuse = float3(0.0f, 0.0f, 0.0f);
	float3 specular = float3(0.0f, 0.0f, 0.0f);

	// Compute Colour
	float3 LightVec = -light.Direction;
	// Compute the reflection vector.
	float3 r = reflect(-LightVec, InNormal);

	// Determine how much specular light makes it into the eye.
	float specularAmount = pow(max(dot(r, ToEye), 0.0f), light.Specular.w);

	// Determine the diffuse light intensity that strikes the vertex.
	float diffuseAmount = max(dot(LightVec, InNormal), 0.0f);

	// Only display specular when there is diffuse
	if (diffuseAmount <= 0.0f)
	{
		specularAmount = 0.0f;
	}

	// Compute the ambient, diffuse, and specular terms separately.
	specular += specularAmount * (surface.Specular * light.Specular).rgb;
	diffuse += diffuseAmount * (surface.Diffuse * light.Diffuse).rgb;
	ambient += (surface.Ambient * light.Ambient).rgb;

#ifdef AO
	float AmbientAccess = CalculateAmbientAccess(InNormal, InPosition, TexCoord);

	ambient *= AmbientAccess;
#endif
	// Sum all the terms together and copy over the diffuse alpha.
	float4 finalColour;

	if (HasTexture == 1.0f)
	{
		finalColour.rgb = (InDiffuse * (ambient + diffuse)) + specular;
	}
	else
	{
		finalColour.rgb = ambient + diffuse + specular;
	}

	finalColour.a = surface.Diffuse.a;

	return finalColour;
}

//Texture2D Normals_Def : register(t4);
//Texture2D Diffuse_Def : register(t5);
//Texture2D Specular_Def : register(t6);
//Texture2D Position_Def : register(t7);
float4 PS_DEFERRED_LIGHTS(float4 ScreenPos : SV_Position, float2 TexCoord : TEXCOORD0) : SV_Target
{
	float3 Normal;
	float3 Position;
	float3 Diffuse;
	float3 Specular;
	float SpecPower;

	//GetGBufferAtrributes(TexCoord, Normal, Position, Diffuse, Specular, SpecPower);
	GetGBufferAtrributes(ScreenPos.xy, Normal, Position, Diffuse, Specular, SpecPower);

	float3 Lighting = CalculateLighting(Normal, Position, Diffuse, Specular, SpecPower, TexCoord);

	//float4 Colour = float4(Lighting, 0.0f) + surface.Diffuse;

	return float4(Lighting, 1.0f);
}


#ifdef SHADOW_MAP

cbuffer ShadowCB : register (b3)
{
	float4 ShadowTransform;
}

SamplerComparisonState ShadowSampler
{
	Filter = COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
	AdressU = BORDER;
	AdressV = BORDER;
	AdressW = BORDER;

	BorderColor = float4(0.0f, 0.0f, 0.0f, 0.0f);

	ComparisonFunc = LESS;
};

struct VS_SHADOW_OUT
{
	float4 Position_H : SV_POSITION;
	float3 Position_W : POSITION;
	float3 Normal_W : NORMAL;
	float2 TexCoords : TEXCOORD0;
	float4 ShadowPosition_H : TEXCOORD1;

}; 

VS_SHADOW_OUT VS_SHADOW(VS_INPUT Input)
{
	VS_SHADOW_OUT Output;

	Output.Position_W = mul(float4(Input.PosL, 1.0f), World).xyz;
	Output.Normal_W = mul(float4(Input.NormL, 1.0f), World).xyz;

	Output.Position_H = mul(float4(Input.PosL, 1.0f), World);
	Output.Position_H = mul(Output.Position_H, View);
	Output.Position_H = mul(Output.Position_H, Projection);

	Output.ShadowPosition_H = mul(float4(Input.PosL, 1.0f), ShadowTransform);

	Output.TexCoords = Input.Tex;

	return Output;

}

float4 PS_SHADOW(VS_SHADOW_OUT Input) : SV_Target
{


	float3 toEye = normalize(EyePosW - Input.Position_W);
	float3 FromEye = -toEye;

	float3 Normal = normalize(Input.Normal_W);

	float2 FinalCoords = Input.TexCoords;// +(toEye.xy * BiasedHeight);


	//// Get texture data from file
	float4 textureColour = txDiffuse.Sample(samLinear, FinalCoords);

	float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 Shadow = CalculateShadowFactor(ShadowSampler, txShadow, Input.Position_H);




	[unroll]
	for (int i = 0; i < 1; ++i)
	{
		float4 A, D, S;
		ComputeDirectionalLight(surface, light, Normal.xyz, toEye, A, D, S);

		ambient += A;
		diffuse += D * Shadow[i];
		specular += S * Shadow[i];
	}


	// Sum all the terms together and copy over the diffuse  alpha.
	float4 finalColour;

	if (HasTexture == 1.0f)
	{
		finalColour.rgb = (textureColour.rgb * (ambient + diffuse)) + specular;

	}
	else
	{
		finalColour.rgb = ambient + diffuse + specular;

	}


	finalColour.a = surface.Diffuse.a;

	return finalColour;

	//float4 Thing = float4(0.0f, 0.0f, 0.0f, 0.0f);
	//return Thing;
}


#endif