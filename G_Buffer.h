#pragma once
#include <d3d11_1.h>
#include <directxmath.h>
#include "FullScreenRenderTarget.h"

using namespace DirectX;

class G_Buffer
{
public:
	G_Buffer(ID3D11Device* InDevice, ID3D11DeviceContext* InImmediateContext, UINT InWidth, UINT InHeight);
	~G_Buffer();

	HRESULT InitVertexBuffers();
	HRESULT InitIndexBuffers();

	HRESULT InitInputLayoutAndShader(ID3DBlob* VS_Blob);

	void SetOMRenderTargets();

	void RenderFromTextures(ID3D11PixelShader* PixelShader_DeferredLightPass);

private:
	FullScreenRenderTarget* NormalMap;
	FullScreenRenderTarget* DiffuseAlbedoMap;
	FullScreenRenderTarget* SpecularAlbedoMap;
	FullScreenRenderTarget* PositionMap;

	ID3D11InputLayout*      Layout = nullptr;
	ID3D11Buffer*           VertexBuffer = nullptr;
	ID3D11Buffer*           IndexBuffer = nullptr;

	ID3D11VertexShader* VertexShader;

	ID3D11Device* Device;
	ID3D11DeviceContext* ImmediateContext;
};



