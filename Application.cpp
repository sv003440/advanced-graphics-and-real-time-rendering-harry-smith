#include "Application.h"
#include<iostream>
#include <WindowsX.h>


namespace
{
	// This is just used to forward Windows messages from a global window
	// procedure to our member function window procedure because we cannot
	// assign a member function to WNDCLASS::lpfnWndProc.
	Application* App = 0;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	return App->MessageProcedure( hWnd,  message,  wParam,  lParam);
	
}

LRESULT Application::MessageProcedure(HWND HWnd, UINT Message, WPARAM WParam, LPARAM LParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (Message)
	{
	case WM_PAINT:
		hdc = BeginPaint(HWnd, &ps);
		EndPaint(HWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_LBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_RBUTTONDOWN:

		OnMouseDown(WParam, GET_X_LPARAM(LParam), GET_Y_LPARAM(LParam));
		return true;
	case WM_LBUTTONUP:
	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
		OnMouseUp(WParam, GET_X_LPARAM(LParam), GET_Y_LPARAM(LParam));
		return true;
	case WM_MOUSEMOVE:
		OnMouseMove(WParam, GET_X_LPARAM(LParam), GET_Y_LPARAM(LParam));
		return true;


	default:
		return DefWindowProc(HWnd, Message, WParam, LParam);
	}

	return 0;
}

bool Application::HandleKeyboard(MSG Message)
{
	switch (Message.wParam)
	{
	case VK_UP:
		_cameraOrbitRadius = max(_cameraOrbitRadiusMin, _cameraOrbitRadius - (_cameraSpeed * 0.2f));
		return true;
		break;

	case VK_DOWN:
		_cameraOrbitRadius = min(_cameraOrbitRadiusMax, _cameraOrbitRadius + (_cameraSpeed * 0.2f));
		return true;
		break;

	case VK_RIGHT:
		_cameraOrbitAngleXZ -= _cameraSpeed;
		return true;
		break;

	case VK_LEFT:
		_cameraOrbitAngleXZ += _cameraSpeed;
		return true;
		break;

	//case VK_HOME:



	case VK_ESCAPE:
	case VK_BACK:
		PostQuitMessage(88);
		break;
	}

	return false;
}

Application::Application()
{
	_hInst = nullptr;
	_hWnd = nullptr;
	_driverType = D3D_DRIVER_TYPE_NULL;
	_featureLevel = D3D_FEATURE_LEVEL_11_0;
	Device = nullptr;
	ImmediateContext = nullptr;
	SwapChain = nullptr;
	BackBufferRTV = nullptr;
	VertexShader = nullptr;
	PixelShader = nullptr;
	VertexLayout = nullptr;
	VertexBuffer_Box = nullptr;
	IndexBuffer_Box = nullptr;
	ConstantBuffer = nullptr;

	DSLessEqual = nullptr;
	RSCullNone = nullptr;

	App = this;

}

Application::~Application()
{
	Cleanup();
}

HRESULT Application::Initialise(HINSTANCE hInstance, int nCmdShow)
{
    if (FAILED(InitWindow(hInstance, nCmdShow)))
	{
        return E_FAIL;
	}

    RECT rc;
    GetClientRect(_hWnd, &rc);
    WindowWidth = rc.right - rc.left;
    WindowHeight = rc.bottom - rc.top;

    if (FAILED(InitDevice()))
    {
        Cleanup();

        return E_FAIL;
    }

	//CreateDDSTextureFromFile(_pd3dDevice, L"Resources\\stone.dds", nullptr, &_pTextureRV);
	CreateDDSTextureFromFile(Device, L"Resources\\floor.dds", nullptr, &TextureRV_Floor);

	CreateDDSTextureFromFile(Device, L"Resources\\Crate Textures\\Crate_COLOR.dds", nullptr, &Color_Box);
	CreateDDSTextureFromFile(Device, L"Resources\\Crate Textures\\Crate_DISP.dds", nullptr, &Displacement_Box);
	CreateDDSTextureFromFile(Device, L"Resources\\Crate Textures\\Crate_NRM.dds", nullptr, &Normals_Box);
	CreateDDSTextureFromFile(Device, L"Resources\\Crate Textures\\Crate_OCC.dds", nullptr, &Occlusion_Box);

	CreateDDSTextureFromFile(Device, L"Resources\\stones.dds", nullptr, &Stones_Diffuse);
	CreateDDSTextureFromFile(Device, L"Resources\\stones_nmap.dds", nullptr, &Stones_Normal);

	CreateDDSTextureFromFile(Device, L"Resources\\bricks.dds", nullptr, &Bricks_Diffuse);
	CreateDDSTextureFromFile(Device, L"Resources\\bricks_nmap.dds", nullptr, &Bricks_Normal);
	CreateDDSTextureFromFile(Device, L"Resources\\RandomNormals.dds", nullptr, &RandomNormals);

    // Setup Camera
	XMFLOAT3 eye = XMFLOAT3(0.0f, 2.0f, -1.0f);
	XMFLOAT3 at = XMFLOAT3(0.0f, 2.0f, 0.0f);
	XMFLOAT3 up = XMFLOAT3(0.0f, 1.0f, 0.0f);

	MainCamera = new Camera(eye, at, up, (float)RenderWidth, (float)RenderHeight, 0.01f, 200.0f);

	// Setup the scene's light
	DirLight.Ambient = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
	DirLight.Diffuse = XMFLOAT4(1.2f, 1.2f, 1.2f, 1.0f);
	DirLight.Specular = XMFLOAT4(0.4f, 0.4f, 0.4f, 5.0f);
	//DirLight.SpecularPower = 20.0f;
	DirLight.Direction = XMFLOAT3(1.0f, -1.0f, 0.0f);

	Geometry cubeGeometry;
	cubeGeometry.indexBuffer = IndexBuffer_Box;
	cubeGeometry.vertexBuffer = VertexBuffer_Box;
	cubeGeometry.numberOfIndices = 36;
	cubeGeometry.vertexBufferOffset = 0;
	cubeGeometry.vertexBufferStride = sizeof(vertex_simple);

	Geometry planeGeometry;
	planeGeometry.indexBuffer = IndexBuffer_Plane;
	planeGeometry.vertexBuffer = VertexBuffer_Plane;
	planeGeometry.numberOfIndices = 6;
	planeGeometry.vertexBufferOffset = 0;
	planeGeometry.vertexBufferStride = sizeof(vertex_simple);

	Material shinyMaterial;
	shinyMaterial.ambient = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	shinyMaterial.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	shinyMaterial.specular = XMFLOAT4(0.5f, 0.5f, 0.5f, 10.0f);
	shinyMaterial.Reflect = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	//shinyMaterial.specularPower = 10.0f;

	Material noSpecMaterial;
	noSpecMaterial.ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	noSpecMaterial.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	noSpecMaterial.specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	shinyMaterial.Reflect = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	//noSpecMaterial.specularPower = 0.0f;
	
	GameObject * gameObject = new GameObject("Floor", planeGeometry, noSpecMaterial);
	gameObject->SetPosition(0.0f, 0.0f, 0.0f);
	gameObject->SetScale(15.0f, 15.0f, 15.0f);
	gameObject->SetRotation(XMConvertToRadians(90.0f), 0.0f, 0.0f);
	gameObject->SetTextureRV(TextureRV_Floor);

	GameObjects.push_back(gameObject);

	int NumOfCubes = 6;

	for (auto i = 0; i < NumOfCubes; i++)
	{
		gameObject = new GameObject("Cube", cubeGeometry, shinyMaterial);
		gameObject->SetScale(0.5f, 0.5f, 0.5f);
		gameObject->SetPosition(-4.0f + (i * 2.0f), i + 0.5f, 1.0f);
		//gameObject->SetRotation(XMConvertToRadians(90.0f), i*10.0f, i*10.0f);
		//gameObject->SetRotation(XMConvertToRadians(90.0f), 0.0f, 0.0f);
		gameObject->SetRotation(0.0f, 0.0f, 0.0f);


		gameObject->SetTextureRV(Color_Box);

		GameObjects.push_back(gameObject);
	}

	LastMousePosition.x = 0;
	LastMousePosition.y = 0;

	SceneBounds = new BoundingSphere(XMFLOAT3(0.0f, 0.0f, 0.0f), sqrtf(10.0f*10.0f + 15.0f*15.0f));
	

	ShadowMap->BuildShadowTransform(*SceneBounds, &DirLight);

	return S_OK;
}

HRESULT Application::InitShadersAndInputLayout()
{
	HRESULT hr;

    // Compile the vertex shader
    ID3DBlob* pVSBlob = nullptr;
    hr = CompileShaderFromFile(L"DX11 Framework.fx", "VS", "vs_4_0", &pVSBlob);

    if (FAILED(hr))
    {
        MessageBox(nullptr,
                   L"The FX file cannot be compiled.  Failed at compiling basic VS.", L"Error", MB_OK);
        return hr;
    }

	// Create the vertex shader
	hr = Device->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &VertexShader);

	if (FAILED(hr))
	{	
		pVSBlob->Release();
        return hr;
	}

	// Compile the vertex shader for Screen Space FX
	ID3DBlob* VS_RENDERTOTEX_Blob = nullptr;
	hr = CompileShaderFromFile(L"DX11 Framework.fx", "VS_RENDERTOTEX", "vs_4_0", &VS_RENDERTOTEX_Blob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to comile for VS_DEFFERRED.", L"Error", MB_OK);
		return hr;
	}

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
    hr = CompileShaderFromFile(L"DX11 Framework.fx", "PS", "ps_4_0", &pPSBlob);

    if (FAILED(hr))
    {
        MessageBox(nullptr,
                   L"Failed to compile for PS.", L"Error", MB_OK);
        return hr;
    }

	// Create the pixel shader 
	hr = Device->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &PixelShader);
	pPSBlob->Release();

    if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to create pixel shader", L"Error", MB_OK);

		return hr;
	}

	
	// Compile the pixel shader for Screen Space FX
	ID3DBlob* PS_RENDERTOTEX_Blob = nullptr;
	hr = CompileShaderFromFile(L"DX11 Framework.fx", "PS_RENDERTOTEX", "ps_4_0", &PS_RENDERTOTEX_Blob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to compile for PS_RENDERTOTEX.", L"Error", MB_OK);
		PS_RENDERTOTEX_Blob->Release();

		return hr;
	}

	// Compile the pixel shaders for actual Deferred rendring
	ID3DBlob* PS_DEFERREDPREPASS_Blob = nullptr;
	hr = CompileShaderFromFile(L"DX11 Framework.fx", "PS_DEFERRED_PRE", "ps_4_0", &PS_DEFERREDPREPASS_Blob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to compile for PS_DEFERREDPREPASS.", L"Error", MB_OK);
		PS_DEFERREDPREPASS_Blob->Release();

		return hr;
	}
	
	hr = Device->CreatePixelShader(PS_DEFERREDPREPASS_Blob->GetBufferPointer(), PS_DEFERREDPREPASS_Blob->GetBufferSize(), nullptr, &PixelShader_DeferredPrePass);
	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to CreatePixelShader for PixelShader_DeferredPrePass.", L"Error", MB_OK);
		PS_DEFERREDPREPASS_Blob->Release();

		return hr;
	}

	ID3DBlob* PS_DEFERREDLIGHTPASS_Blob = nullptr;
	hr = CompileShaderFromFile(L"DX11 Framework.fx", "PS_DEFERRED_LIGHTS", "ps_4_0", &PS_DEFERREDLIGHTPASS_Blob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to compile for PS_DEFERREDLIGHTPASS.", L"Error", MB_OK);
		PS_DEFERREDLIGHTPASS_Blob->Release();

		return hr;
	}

	hr = Device->CreatePixelShader(PS_DEFERREDLIGHTPASS_Blob->GetBufferPointer(), PS_DEFERREDLIGHTPASS_Blob->GetBufferSize(), nullptr, &PixelShader_DeferredLightPass);
	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to CreatePixelShader for PixelShader_DeferredLightPass.", L"Error", MB_OK);
		PS_DEFERREDLIGHTPASS_Blob->Release();

		return hr;
	}

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },

	};

	UINT numElements = ARRAYSIZE(layout);

    // Create the input layout
	hr = Device->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
                                        pVSBlob->GetBufferSize(), &VertexLayout);
	pVSBlob->Release();

	if (FAILED(hr))
        return hr;

    // Set the input layout
    ImmediateContext->IASetInputLayout(VertexLayout);

	//TODO: Put some proper checking in place herea as well
	hr = DeferredRenderTarget->InitShadersAndInputLayout(VS_RENDERTOTEX_Blob, PS_RENDERTOTEX_Blob);

	hr = GBuffer->InitInputLayoutAndShader(VS_RENDERTOTEX_Blob);

	VS_RENDERTOTEX_Blob->Release();
	PS_RENDERTOTEX_Blob->Release();

	if (FAILED(hr))
		return hr;

	//compile Shadow Vertex Shader

	 hr = CompileShaderFromFile(L"DX11 Framework.fx", "VS_SHADOW", "vs_4_0", &VS_SHADOWS_Blob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to compile for VS_SHADOW.", L"Error", MB_OK);
		return hr;
	}

	// Compile the pixel shader for Shadows

	hr = CompileShaderFromFile(L"DX11 Framework.fx", "PS_SHADOW", "ps_4_0", &PS_SHADOWS_Blob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to compile for PS_SHADOW.", L"Error", MB_OK);
		return hr;
	}

	hr = ShadowMap->InitShadersAndInputLayout(VS_SHADOWS_Blob, PS_SHADOWS_Blob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to InitShadersAndInputLayout for PS_SHADOW.", L"Error", MB_OK);
		return hr;
	}


	///////////////
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;//D3D11_FILTER_ANISOTROPIC;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = Device->CreateSamplerState(&sampDesc, &LinearSampler);

	return hr;
}

HRESULT Application::InitVertexBuffer()
{
	HRESULT hr;

    // Create vertex buffer
    vertex_simple vertices[] = //Position, Normal, TexCoord, Tangent
    {
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },//FRONT//

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(-1.0f, .0f, .0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) , XMFLOAT3(-1.0f, .0f, .0f) },//BACK//
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) , XMFLOAT3(-1.0f, .0f, .0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(-1.0f, .0f, .0f) },

		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },//changed to Luna
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },//TOP FACE//

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) , XMFLOAT3(-1.0f, 0.0f, 0.0f) }, //BOTTOM FACE//
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },//changed to Luna
		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) ,XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 0.0f),XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f) }, //LEFT FACE//

		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) }, //RIGHT FACE//
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },

    };

    D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(vertex_simple) * 24;
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

    D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
    InitData.pSysMem = vertices;

    hr = Device->CreateBuffer(&bd, &InitData, &VertexBuffer_Box);

    if (FAILED(hr))
        return hr;

	// Create vertex buffer
	vertex_simple planeVertices[] =
	{
		{ XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 5.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(5.0f, 5.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(5.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
	};

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(vertex_simple) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = planeVertices;

	hr = Device->CreateBuffer(&bd, &InitData, &VertexBuffer_Plane);

	if (FAILED(hr))
		return hr;

	hr = DeferredRenderTarget->InitVertexBuffer();

	if (FAILED(hr))
		return hr;

	hr = GBuffer->InitVertexBuffers();
	if (FAILED(hr))
		return hr;

	return S_OK;
}

HRESULT Application::InitIndexBuffer()
{
	HRESULT hr;

    // Create index buffer
    WORD indices[] =
    {
		0,1,2,
		0,2,3,

		4,5,6,
		4,6,7,

		8,9,10,
		8,10,11,
		
		12,13,14,
		12,14,15,

		16,17,18,
		16,18,19,

		20,21,22,
		20,22,23
    };

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(WORD) * 36;     
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
    InitData.pSysMem = indices;

    hr = Device->CreateBuffer(&bd, &InitData, &IndexBuffer_Box);

    if (FAILED(hr))
        return hr;

	// Create plane index buffer
	WORD planeIndices[] =
	{
		0, 3, 1,
		3, 2, 1,
	};

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = planeIndices;
	hr = Device->CreateBuffer(&bd, &InitData, &IndexBuffer_Plane);

	hr = DeferredRenderTarget->InitIndexBuffer();

	if (FAILED(hr))
		return hr;

	hr = GBuffer->InitIndexBuffers();
	if (FAILED(hr))
		return hr;


	return S_OK;
}

HRESULT Application::InitWindow(HINSTANCE hInstance, int nCmdShow)
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_TUTORIAL1);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW );
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = nullptr;
    wcex.lpszClassName = L"TutorialWindowClass";
    wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
    if (!RegisterClassEx(&wcex))
        return E_FAIL;

    // Create window
    _hInst = hInstance;
    RECT rc = {0, 0, 960, 540};
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
    _hWnd = CreateWindow(L"TutorialWindowClass", L"Advanced Garphics and Real-Time Rendering", WS_OVERLAPPEDWINDOW,
                         CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance,
                         nullptr);
    if (!_hWnd)
		return E_FAIL;

    ShowWindow(_hWnd, nCmdShow);

    return S_OK;
}

HRESULT Application::CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile(szFileName, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, szEntryPoint, szShaderModel,
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
	//(L"Lights.fx")
    if (FAILED(hr))
    {
        if (pErrorBlob != nullptr)
            OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());

        if (pErrorBlob) pErrorBlob->Release();

        return hr;
    }

    if (pErrorBlob) pErrorBlob->Release();

    return S_OK;
}

HRESULT Application::InitDevice()
{
    HRESULT hr = S_OK;

    UINT createDeviceFlags = 0;

#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };

    UINT numDriverTypes = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };

	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	UINT sampleCount = 4;

    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 1;
    sd.BufferDesc.Width = RenderWidth;
    sd.BufferDesc.Height = RenderHeight;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = _hWnd;
	sd.SampleDesc.Count = sampleCount;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
    {
        _driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain(nullptr, _driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
                                           D3D11_SDK_VERSION, &sd, &SwapChain, &Device, &_featureLevel, &ImmediateContext);
        if (SUCCEEDED(hr))
            break;
    }

    if (FAILED(hr))
        return hr;

    // Create a render target view
    ID3D11Texture2D* pBackBuffer = nullptr;
    hr = SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

    if (FAILED(hr))
        return hr;

    hr = Device->CreateRenderTargetView(pBackBuffer, nullptr, &BackBufferRTV);
    pBackBuffer->Release();

    if (FAILED(hr))
        return hr;


    // Setup the viewport
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)RenderWidth;
    vp.Height = (FLOAT)RenderHeight;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    ImmediateContext->RSSetViewports(1, &vp);
	
	DeferredRenderTarget = new FullScreenRenderTarget(Device, ImmediateContext, RenderWidth, RenderHeight);

	GBuffer = new G_Buffer(Device, ImmediateContext, RenderWidth, RenderHeight);

	ShadowMap = new ShadowMapper(Device, ImmediateContext, ShadowMapSize, ShadowMapSize);

	InitShadersAndInputLayout();

	InitVertexBuffer();
	InitIndexBuffer();

    // Set primitive topology
    ImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(constant_buffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
    hr = Device->CreateBuffer(&bd, nullptr, &ConstantBuffer);

    if (FAILED(hr))
        return hr;

	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width = RenderWidth;
	depthStencilDesc.Height = RenderHeight;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count = sampleCount;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	Device->CreateTexture2D(&depthStencilDesc, nullptr, &DepthStencilBuffer);
	Device->CreateDepthStencilView(DepthStencilBuffer, nullptr, &DepthStencilView);

	ImmediateContext->OMSetRenderTargets(1, &BackBufferRTV, DepthStencilView);


	// Rasterizer
	D3D11_RASTERIZER_DESC cmdesc;

	ZeroMemory(&cmdesc, sizeof(D3D11_RASTERIZER_DESC));
	cmdesc.FillMode = D3D11_FILL_SOLID;
	cmdesc.CullMode = D3D11_CULL_NONE;
	hr = Device->CreateRasterizerState(&cmdesc, &RSCullNone);

	D3D11_DEPTH_STENCIL_DESC dssDesc;
	ZeroMemory(&dssDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	dssDesc.DepthEnable = true;
	dssDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dssDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;

	Device->CreateDepthStencilState(&dssDesc, &DSLessEqual);

	ZeroMemory(&cmdesc, sizeof(D3D11_RASTERIZER_DESC));

	cmdesc.FillMode = D3D11_FILL_SOLID;
	cmdesc.CullMode = D3D11_CULL_BACK;

	cmdesc.FrontCounterClockwise = true;
	hr = Device->CreateRasterizerState(&cmdesc, &CCWcullMode);

	cmdesc.FrontCounterClockwise = false;
	hr = Device->CreateRasterizerState(&cmdesc, &CWcullMode);

    return S_OK;
}

void Application::Cleanup()
{
    if (ImmediateContext) ImmediateContext->ClearState();
	if (LinearSampler) LinearSampler->Release();

	if (TextureRV_Box) TextureRV_Box->Release();

	if (Color_Box) Color_Box->Release();
	if (Displacement_Box) Displacement_Box->Release();
	if (Normals_Box) Normals_Box->Release();
	if (Occlusion_Box) Occlusion_Box->Release();

	if (Stones_Diffuse) Stones_Diffuse->Release();
	if (Stones_Normal) Stones_Normal->Release();	
	if (Bricks_Diffuse) Bricks_Diffuse->Release();
	if (Bricks_Normal) Bricks_Normal->Release();
	if (RandomNormals) 	RandomNormals->Release();

	 
	if (TextureRV_Floor) TextureRV_Floor->Release();

    if (ConstantBuffer) ConstantBuffer->Release();

    if (VertexBuffer_Box) VertexBuffer_Box->Release();
    if (IndexBuffer_Box) IndexBuffer_Box->Release();
	if (VertexBuffer_Plane) VertexBuffer_Plane->Release();
	if (IndexBuffer_Plane) IndexBuffer_Plane->Release();

	if (DeferredRenderTarget)DeferredRenderTarget->~FullScreenRenderTarget();
	if(GBuffer)GBuffer->~G_Buffer();
	if (PixelShader_DeferredPrePass) PixelShader_DeferredPrePass->Release();
	if (PixelShader_DeferredLightPass) PixelShader_DeferredLightPass->Release();

	if (ShadowMap)ShadowMap->~ShadowMapper();





    if (VertexLayout) VertexLayout->Release();
    if (VertexShader) VertexShader->Release();
    if (PixelShader) PixelShader->Release();
    if (BackBufferRTV) BackBufferRTV->Release();
    if (SwapChain) SwapChain->Release();
    if (ImmediateContext) ImmediateContext->Release();
    if (Device) Device->Release();
	if (DepthStencilView) DepthStencilView->Release();
	if (DepthStencilBuffer) DepthStencilBuffer->Release();

	if (DSLessEqual) DSLessEqual->Release();
	if (RSCullNone) RSCullNone->Release();

	if (CCWcullMode) CCWcullMode->Release();
	if (CWcullMode) CWcullMode->Release();

	if (MainCamera)
	{
		delete MainCamera;
		MainCamera = nullptr;
	}

	for (auto gameObject : GameObjects)
	{
		if (gameObject)
		{
			delete gameObject;
			gameObject = nullptr;
		}
	}
}

void Application::MoveForward(int objectNumber)
{
	XMFLOAT3 position = GameObjects[objectNumber]->GetPosition();
	position.z -= 0.1f;
	GameObjects[objectNumber]->SetPosition(position);
}

void Application::Update()
{
    // Update our time
    static float timeSinceStart = 0.0f;
    static DWORD dwTimeStart = 0;

    DWORD dwTimeCur = GetTickCount();

    if (dwTimeStart == 0)
        dwTimeStart = dwTimeCur;

	timeSinceStart = (dwTimeCur - dwTimeStart) / 1000.0f;

	// Move gameobject
	if (GetAsyncKeyState('1'))
	{
		MoveForward(1);
	}

	// Update camera


	MainCamera->SetPosition(Radius);
	MainCamera->Update();

	// Update objects
	for (auto gameObject : GameObjects)
	{
		gameObject->Update(timeSinceStart);

		if (gameObject->GetType() == "Cube")
		{
			gameObject->SetRotation(0.0f, XMConvertToRadians(timeSinceStart * 20), 0.0f);
		}

	}
}

#define DEFERRED
void Application::Draw()
{
	constant_buffer ConstantBuffer_CPU;

	XMFLOAT4X4 viewAsFloats = MainCamera->GetView();
	XMFLOAT4X4 projectionAsFloats = MainCamera->GetProjection();

	XMMATRIX view = XMLoadFloat4x4(&viewAsFloats);
	XMMATRIX projection = XMLoadFloat4x4(&projectionAsFloats);

	ConstantBuffer_CPU.View = XMMatrixTranspose(view);
	ConstantBuffer_CPU.Projection = XMMatrixTranspose(projection);

	ConstantBuffer_CPU.Light = DirLight;
	ConstantBuffer_CPU.EyePosW = MainCamera->GetPosition();

	//Shadow Mapping attempt
	////ShadowMap->BindDepthStencilView();

	////ShadowMap->DrawScene();

    //
    // Setup buffers and render scene
    //
#ifdef DEFERRED
	GBuffer->SetOMRenderTargets();

#else
	DeferredRenderTarget->SetOMRenderTarget();

#endif // DEFERRED


	ImmediateContext->IASetInputLayout(VertexLayout);

	ImmediateContext->VSSetShader(VertexShader, nullptr, 0);
#ifdef DEFERRED
	ImmediateContext->PSSetShader(PixelShader_DeferredPrePass, nullptr, 0);
#else
	ImmediateContext->PSSetShader(PixelShader, nullptr, 0);
#endif

	ImmediateContext->VSSetConstantBuffers(0, 1, &ConstantBuffer);
	ImmediateContext->PSSetConstantBuffers(0, 1, &ConstantBuffer);
	ImmediateContext->PSSetSamplers(0, 1, &LinearSampler);

	DrawSceneObjects(ConstantBuffer_CPU);
	
	ImmediateContext->OMSetRenderTargets(1, &BackBufferRTV, DepthStencilView);

	float ClearColor[4] = { 0.5f, 0.5f, 0.5f, 1.0f }; // red,green,blue,alpha
	ImmediateContext->ClearRenderTargetView(BackBufferRTV, ClearColor);
	ImmediateContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	ImmediateContext->PSSetSamplers(0, 1, &LinearSampler);

#ifdef DEFERRED
	ImmediateContext->PSSetShaderResources(8, 1, &RandomNormals);
	GBuffer->RenderFromTextures(PixelShader_DeferredLightPass);
#else
	DeferredRenderTarget->RenderFromTexture(LinearSampler);
#endif


	//tempShaderRV->Release();
	//From Frank Luna
	// Unbind shadow map and AmbientMap as a shader input because we are going to render
	// to it next frame.  These textures can be at any slot, so clear all slots.
	ID3D11ShaderResourceView* nullSRV[16] = { 0 };
	ImmediateContext->PSSetShaderResources(0, 16, nullSRV);

    //
    // Present our back buffer to our front buffer
    //

	HRESULT hr;
    hr = SwapChain->Present(0, 0);

	if (FAILED(hr))
	{
		std::cerr << "Swap chain failed. Error code: " << HRESULT_CODE(hr);
	}
}


void Application::OnMouseDown(WPARAM State, int x, int y)
{

	LastMousePosition.x = x;
	LastMousePosition.y = y;

	std::cerr << "Mouse down" ;

	SetCapture(_hWnd);
}

void Application::OnMouseUp(WPARAM State, int x, int y)
{
	ReleaseCapture();
}

void Application::OnMouseMove(WPARAM State, int x, int y)
{
	if ((State & MK_LBUTTON) != 0)
	{
		float dx = XMConvertToRadians(0.25f*static_cast<float>(x - LastMousePosition.x));
		float dy = XMConvertToRadians(0.25f*static_cast<float>(y - LastMousePosition.y));

		MainCamera->Theta += dx;
		MainCamera->Phi += dy;

		MainCamera->Phi = MathHelper::Clamp(MainCamera->Phi, 0.1f, MathHelper::Pi - 0.1f);
	}
	else if ((State & MK_RBUTTON) != 0)
	{
		// Make each pixel correspond to 0.01 unit in the scene.
		float dx = 0.01f*static_cast<float>(x - LastMousePosition.x);
		float dy = 0.01f*static_cast<float>(y - LastMousePosition.y);

		// Update the camera radius based on input.
		Radius += dx - dy;

		// Restrict the radius.
		Radius = MathHelper::Clamp(Radius, 1.0f, 15.0f);
	}

	LastMousePosition.x = x;
	LastMousePosition.y = y;

}

void Application::DrawSceneObjects(constant_buffer& InOutCB)
{
	// Render all scene objects
	for (auto gameObject : GameObjects)
	{
		// Get render material
		Material material = gameObject->GetMaterial();

		// Copy material to shader
		InOutCB.Material.ambient = material.ambient;
		InOutCB.Material.diffuse = material.diffuse;
		InOutCB.Material.specular = material.specular;

		//cb.Material = gameObject->GetMaterial();

		// Set world matrix
		InOutCB.World = XMMatrixTranspose(gameObject->GetWorldMatrix());

		// Set texture
		if (gameObject->GetType() == "Floor" && gameObject->HasTexture())
		{
			//ID3D11ShaderResourceView * textureRV = gameObject->GetTextureRV();
			//ImmediateContext->PSSetShaderResources(0, 1, &textureRV);
			ImmediateContext->PSSetShaderResources(0, 1, &Bricks_Diffuse);
			ImmediateContext->PSSetShaderResources(2, 1, &Bricks_Normal);

			InOutCB.HasTexture = 1.0f;
		}
		else if (gameObject->GetType() == "Cube" && gameObject->HasTexture())
		{
			//ID3D11ShaderResourceView * textureRV = gameObject->GetTextureRV();
			//	_pImmediateContext->VSSetShaderResources(1, 1, &CrateNormals);
			//ID3D11ShaderResourceView* TextureRVs[] = { textureRV, CrateDisplacement, CrateNormals };
			/*_pImmediateContext->PSSetShaderResources(0, 1, &textureRV);
			_pImmediateContext->PSSetShaderResources(1, 1, &CrateDisplacement);
			_pImmediateContext->PSSetShaderResources(2, 1, &CrateNormals);*/
			ImmediateContext->PSSetShaderResources(0, 1, &Stones_Diffuse);
			ImmediateContext->PSSetShaderResources(2, 1, &Stones_Normal);


			//_pImmediateContext->PSSetShaderResources(0, 3, TextureRVs);
			InOutCB.HasTexture = 1.0f;
		}
		else
		{
			InOutCB.HasTexture = 0.0f;
		}

		// Update constant buffer
		ImmediateContext->UpdateSubresource(ConstantBuffer, 0, nullptr, &InOutCB, 0, 0);

		// Draw object
		gameObject->Draw(ImmediateContext);

	}
}
