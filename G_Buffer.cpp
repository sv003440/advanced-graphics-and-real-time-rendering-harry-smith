#include"G_Buffer.h"

G_Buffer::G_Buffer(ID3D11Device* InDevice, ID3D11DeviceContext* InImmediateContext, UINT InWidth, UINT InHeight): Device(InDevice), ImmediateContext(InImmediateContext)
{
	NormalMap = new FullScreenRenderTarget(InDevice, InImmediateContext, InWidth, InHeight);
	DiffuseAlbedoMap = new FullScreenRenderTarget(InDevice, InImmediateContext, InWidth, InHeight);
	SpecularAlbedoMap = new FullScreenRenderTarget(InDevice, InImmediateContext, InWidth, InHeight);
	PositionMap = new FullScreenRenderTarget(InDevice, InImmediateContext, InWidth, InHeight);

}

G_Buffer::~G_Buffer()
{
	NormalMap->~FullScreenRenderTarget();
	DiffuseAlbedoMap->~FullScreenRenderTarget();
	SpecularAlbedoMap->~FullScreenRenderTarget();
	PositionMap->~FullScreenRenderTarget();

	Layout->Release();
	VertexBuffer->Release();
	IndexBuffer->Release();

	VertexShader->Release();
}

HRESULT G_Buffer::InitVertexBuffers()
{
	HRESULT hr;

	vertex_deferred QuadVerts[] =
	{
		{ XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 0.0f),  XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(vertex_deferred) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = QuadVerts;

	hr = Device->CreateBuffer(&bd, &InitData, &VertexBuffer);

	if (FAILED(hr))
		return hr;

	return hr;
}

HRESULT G_Buffer::InitIndexBuffers()
{

	HRESULT hr;
	// Create Quad index buffer
	WORD QuadIndices[] =
	{
		0, 3, 1,
		3, 2, 1,
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = QuadIndices;

	hr = Device->CreateBuffer(&bd, &InitData, &IndexBuffer);

	if (FAILED(hr))
		return hr;

	return hr;
}

HRESULT G_Buffer::InitInputLayoutAndShader(ID3DBlob* VS_Blob)
{

	//hr = NormalMap->InitInputLayout(VS_Blob);
	//hr = DiffuseAlbedoMap->InitInputLayout(VS_Blob);
	//hr = SpecularAlbedoMap->InitInputLayout(VS_Blob);
	//hr = PositionMap->InitInputLayout(VS_Blob);

	HRESULT hr;

	// Create the vertex shader for Screen Space FX
	hr = Device->CreateVertexShader(VS_Blob->GetBufferPointer(), VS_Blob->GetBufferSize(), nullptr, &VertexShader);

	if (FAILED(hr))
	{
		VS_Blob->Release();
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC LayoutDESC[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }, //DXGI_FORMAT_R32G32B32_FLOAT
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	UINT NumElements = ARRAYSIZE(LayoutDESC);

	// Create the input layout for SSFX
	hr = Device->CreateInputLayout(LayoutDESC, NumElements, VS_Blob->GetBufferPointer(), VS_Blob->GetBufferSize(), &Layout);

	return hr;
}

void G_Buffer::SetOMRenderTargets()
{
	float ClearColor[4] = { 0.5f, 0.5f, 0.5f, 1.0f }; // red,green,blue,alpha

	ID3D11RenderTargetView* RTVs[] = { NormalMap->GetRenderTargetView() , DiffuseAlbedoMap->GetRenderTargetView() ,SpecularAlbedoMap->GetRenderTargetView() ,PositionMap->GetRenderTargetView() };

	ImmediateContext->ClearRenderTargetView(NormalMap->GetRenderTargetView(), ClearColor);
	ImmediateContext->ClearRenderTargetView(DiffuseAlbedoMap->GetRenderTargetView(), ClearColor);
	ImmediateContext->ClearRenderTargetView(SpecularAlbedoMap->GetRenderTargetView(), ClearColor);
	ImmediateContext->ClearRenderTargetView(PositionMap->GetRenderTargetView(), ClearColor);

	ID3D11DepthStencilView* DSV = NormalMap->GetDepthStencilView();

	ImmediateContext->ClearDepthStencilView(DSV, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	ImmediateContext->OMSetRenderTargets(4, RTVs, DSV);
}

void G_Buffer::RenderFromTextures( ID3D11PixelShader* PixelShader_DeferredLightPass)
{
	ImmediateContext->IASetInputLayout(Layout);

	UINT stride = sizeof(vertex_deferred);
	UINT offset = 0;

	ImmediateContext->IASetVertexBuffers(0, 1, &VertexBuffer, &stride, &offset);
	ImmediateContext->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	ImmediateContext->VSSetShader(VertexShader, nullptr, 0);
	ImmediateContext->PSSetShader(PixelShader_DeferredLightPass, nullptr, 0);

	ID3D11ShaderResourceView* SRVs[] = { NormalMap->GetShaderResourceView() , DiffuseAlbedoMap->GetShaderResourceView() ,SpecularAlbedoMap->GetShaderResourceView() ,PositionMap->GetShaderResourceView() };

	ImmediateContext->PSSetShaderResources(4, 4, SRVs);

	ImmediateContext->DrawIndexed(6, 0, 0);
}


//NormalMap
//DiffuseAlbedoMap
//SpecularAlbedoMap
//PositionMap
