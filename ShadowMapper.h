#pragma once
#include <d3d11_1.h>
#include <directxmath.h>

using namespace DirectX;

struct DirectionalLight;

struct ShadowCB
{
	XMMATRIX ShadowWVP;
};

struct BoundingSphere
{
	BoundingSphere(XMFLOAT3 InCentre, float InRadius) : Centre(InCentre), Radius(InRadius) {};

	XMFLOAT3 Centre;
	float Radius;
};

class ShadowMapper
{
public:
	ShadowMapper(ID3D11Device* InDevice, ID3D11DeviceContext* InDeviceContext, UINT InWidth, UINT InHeight);
	~ShadowMapper();

	ID3D11ShaderResourceView* GetDepthMapSRV();

	void BindDepthStencilView();

	void BuildShadowTransform(BoundingSphere SceneBounds, DirectionalLight* DirLight);

	void DrawScene();

	XMFLOAT4X4 LightView4x4;
	XMFLOAT4X4 LightProjection4x4;
	XMFLOAT4X4 ShadowTransform4x4;

	HRESULT InitShadersAndInputLayout(ID3DBlob* VS_SHADOWS_Blob, ID3DBlob* PS_SHADOWS_Blob);

private:

	UINT Width;
	UINT Height;

	ID3D11ShaderResourceView* DepthMapSRV = nullptr;
	ID3D11DepthStencilView* DepthMapDSV = nullptr;

	D3D11_VIEWPORT Viewport;

	ID3D11DeviceContext* DeviceContext;
	ID3D11Device* Device;

	ID3D11VertexShader* VertexShader;
	ID3D11PixelShader* PixelShader;
	ID3D11InputLayout* Layout;

	ID3D11Buffer* ShadowCBuffer;



};


