#include"ShadowMapper.h"
#include<iostream>
#include"Lights.h"

ShadowMapper::ShadowMapper(ID3D11Device* InDevice, ID3D11DeviceContext* InDeviceContext, UINT InWidth, UINT InHeight) : Width(InWidth), Height(InHeight), Device(InDevice), DeviceContext(InDeviceContext)
{
	HRESULT hr;

	Viewport.TopLeftX = 0.0f;
	Viewport.TopLeftY = 0.0f;
	Viewport.Width = static_cast<float>(InWidth);
	Viewport.Height = static_cast<float>(InHeight);
	Viewport.MinDepth = 0.0f;
	Viewport.MaxDepth = 1.0f;

	D3D11_TEXTURE2D_DESC DepthTexDESC;
	DepthTexDESC.Width = InWidth;
	DepthTexDESC.Height = InHeight;
	DepthTexDESC.MipLevels =1;
	DepthTexDESC.ArraySize=1;
	DepthTexDESC.Format = DXGI_FORMAT_R24G8_TYPELESS;
	DepthTexDESC.SampleDesc.Count = 1;
	DepthTexDESC.SampleDesc.Quality = 0;
	DepthTexDESC.Usage = D3D11_USAGE_DEFAULT;
	DepthTexDESC.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	DepthTexDESC.CPUAccessFlags = 0;
	DepthTexDESC.MiscFlags = 0;

	ID3D11Texture2D* DepthMap;

	hr = InDevice->CreateTexture2D(&DepthTexDESC, nullptr, &DepthMap);

	if (FAILED(hr))
	{
		std::cerr << "Error creating Texture2D for DepthMap Error code: " << HRESULT_CODE(hr);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC DepthStencilViewDESC;
	DepthStencilViewDESC.Flags = 0;
	DepthStencilViewDESC.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthStencilViewDESC.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DepthStencilViewDESC.Texture2D.MipSlice = 0;

	hr = InDevice->CreateDepthStencilView(DepthMap, &DepthStencilViewDESC, &DepthMapDSV);
	if (FAILED(hr))
	{
		std::cerr << "Error creating Depth Stencil View for DepthMap Error code: " << HRESULT_CODE(hr);
	}
	
	D3D11_SHADER_RESOURCE_VIEW_DESC DepthShaderResourceViewDESC;
	DepthShaderResourceViewDESC.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	DepthShaderResourceViewDESC.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	DepthShaderResourceViewDESC.Texture2D.MipLevels = DepthTexDESC.MipLevels;
	DepthShaderResourceViewDESC.Texture2D.MostDetailedMip = 0;
	
	hr = InDevice->CreateShaderResourceView(DepthMap, &DepthShaderResourceViewDESC, &DepthMapSRV);
	if (FAILED(hr))
	{
		std::cerr << "Error creating Shader Resource View for DepthMap Error code: " << HRESULT_CODE(hr);
	}

	DepthMap->Release();

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ShadowCB);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	hr = Device->CreateBuffer(&bd, nullptr, &ShadowCBuffer);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to create shadow constant buffer.", L"Error", MB_OK);
	}

}

ShadowMapper::~ShadowMapper()
{
	if(DepthMapSRV) DepthMapSRV->Release();
	if(DepthMapDSV) DepthMapDSV->Release();
	if (VertexShader) VertexShader->Release();
	if (PixelShader) PixelShader->Release();
	if( ShadowCBuffer)  ShadowCBuffer->Release();
	if(Layout) Layout->Release();

}


ID3D11ShaderResourceView* ShadowMapper::GetDepthMapSRV()
{
	return DepthMapSRV;
}


void ShadowMapper::BindDepthStencilView()
{
	DeviceContext->RSSetViewports(1, &Viewport);
	
	ID3D11RenderTargetView* NullRenderTarget[1] = {0};
	//^^Seems unnecesary^^

	//Depth values are all that is needed
	//no need to render colour, so only DepthStencil Buffer is set

	DeviceContext->OMSetRenderTargets(1, NullRenderTarget, DepthMapDSV);
	DeviceContext->ClearDepthStencilView(DepthMapDSV, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void ShadowMapper::BuildShadowTransform(BoundingSphere SceneBounds, DirectionalLight* DirLight)
{
	XMVECTOR LightDirection = XMLoadFloat3(&DirLight->Direction);
	XMVECTOR LightPosition = -2.0f * SceneBounds.Radius * LightDirection;
	XMVECTOR TargetPosition = XMLoadFloat3(&SceneBounds.Centre);

	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX LightViewMatrix = XMMatrixLookAtLH(LightPosition, TargetPosition, Up);

	XMFLOAT3 SphereCentre;

	XMStoreFloat3(&SphereCentre, XMVector3TransformCoord(TargetPosition, LightViewMatrix));

	//ortho frustrum
	float Left = SphereCentre.x - SceneBounds.Radius;
	float Right = SphereCentre.x + SceneBounds.Radius;
	float Bottom = SphereCentre.y - SceneBounds.Radius;
	float Top = SphereCentre.y + SceneBounds.Radius;
	float Near = SphereCentre.z - SceneBounds.Radius;
	float Far = SphereCentre.z + SceneBounds.Radius;

	XMMATRIX LightProjectionMatrix = XMMatrixOrthographicOffCenterLH(Left, Right, Bottom, Top, Near, Far);

	XMMATRIX TransformToTextureMatrix(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, -0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.0f, 1.0f);

	XMMATRIX ShadowTransformMatrix = LightViewMatrix * LightProjectionMatrix * TransformToTextureMatrix;

	XMStoreFloat4x4(&LightView4x4, LightViewMatrix);
	XMStoreFloat4x4(&LightProjection4x4, LightProjectionMatrix);
	XMStoreFloat4x4(&ShadowTransform4x4, ShadowTransformMatrix);
}

void ShadowMapper::DrawScene()
{
	ShadowCB ShadowConstantBuffer;

	XMMATRIX View = XMLoadFloat4x4(&LightView4x4);
	XMMATRIX Projection = XMLoadFloat4x4(&LightProjection4x4);
	XMMATRIX ViewProj =  XMMatrixMultiply(View, Projection);

	ShadowConstantBuffer.ShadowWVP = ViewProj;

	DeviceContext->IASetInputLayout(Layout);

	DeviceContext->VSSetShader(VertexShader, nullptr, 0);
	DeviceContext->PSSetShader(PixelShader, nullptr, 0);

	//ID3D11ShaderResourceView* nullSRV[16] = { 0 };
	//DeviceContext->PSSetShaderResources(0, 16, nullSRV);

	DeviceContext->PSSetShaderResources(3, 1, &DepthMapSRV);

	DeviceContext->VSSetConstantBuffers(3, 1, &ShadowCBuffer);
	DeviceContext->PSSetConstantBuffers(3, 1, &ShadowCBuffer);

	DeviceContext->UpdateSubresource(ShadowCBuffer, 0, nullptr, &ShadowConstantBuffer, 0, 0);
}

HRESULT ShadowMapper::InitShadersAndInputLayout(ID3DBlob* VS_SHADOWS_Blob, ID3DBlob* PS_SHADOWS_Blob)
{
	//create Shadows vert shader
	HRESULT hr = Device->CreateVertexShader(VS_SHADOWS_Blob->GetBufferPointer(), VS_SHADOWS_Blob->GetBufferSize(), nullptr, &VertexShader);
	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to create vertex shader for PS_SHADOW.", L"Error", MB_OK);
		VS_SHADOWS_Blob->Release();
		return hr;
	}


	// Create the pixel shader for Shadows
	hr = Device->CreatePixelShader(PS_SHADOWS_Blob->GetBufferPointer(), PS_SHADOWS_Blob->GetBufferSize(), nullptr, &PixelShader);

	PS_SHADOWS_Blob->Release();

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"Failed to create pixel shader for PS_SHADOW.", L"Error", MB_OK);
		VS_SHADOWS_Blob->Release();
		return hr;
	}

	// Define the input layout for shadows
	D3D11_INPUT_ELEMENT_DESC Shadows_Layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },

	};

	UINT NumElements = ARRAYSIZE(Shadows_Layout);

	// Create the input layout for shadows
	hr = Device->CreateInputLayout(Shadows_Layout, NumElements, VS_SHADOWS_Blob->GetBufferPointer(),
		VS_SHADOWS_Blob->GetBufferSize(), &Layout);
	VS_SHADOWS_Blob->Release();

	return hr;
}
