#include "FullScreenRenderTarget.h"
#include<iostream>

FullScreenRenderTarget::FullScreenRenderTarget( ID3D11Device* InDevice,  ID3D11DeviceContext* InImmediateContext, UINT InWidth, UINT InHeight) : Device(InDevice), ImmediateContext(InImmediateContext)
{
	HRESULT hr;

	//Screen Space Special Effects
	D3D11_TEXTURE2D_DESC FullScreenTextureDESC;
	ZeroMemory(&FullScreenTextureDESC, sizeof(FullScreenTextureDESC));

	FullScreenTextureDESC.Width = InWidth;
	FullScreenTextureDESC.Height = InHeight;
	FullScreenTextureDESC.MipLevels = 1;
	FullScreenTextureDESC.ArraySize = 1;
	FullScreenTextureDESC.Format = DXGI_FORMAT_R8G8B8A8_UNORM;//DXGI_FORMAT_R32G32B32A32_FLOAT
	FullScreenTextureDESC.SampleDesc.Count = 1;//4;
	FullScreenTextureDESC.SampleDesc.Quality = 0;

	FullScreenTextureDESC.Usage = D3D11_USAGE_DEFAULT;// D3D11_USAGE_DYNAMIC;
	FullScreenTextureDESC.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;;
	FullScreenTextureDESC.CPUAccessFlags = 0;// D3D11_CPU_ACCESS_WRITE; //0?
	FullScreenTextureDESC.MiscFlags = 0;

	hr = Device->CreateTexture2D(&FullScreenTextureDESC, nullptr, &FullScreenTexture);

	if (FAILED(hr))
	{
		std::cerr << "Error creating Texture2D for FullScreen RenderTarget Error code: " <<HRESULT_CODE(hr);
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC FSShaderResourceViewDESC;
	FSShaderResourceViewDESC.Format = FullScreenTextureDESC.Format;
	FSShaderResourceViewDESC.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	FSShaderResourceViewDESC.Texture2D.MostDetailedMip = 0;
	FSShaderResourceViewDESC.Texture2D.MipLevels = 1;


	hr = Device->CreateShaderResourceView(FullScreenTexture, &FSShaderResourceViewDESC, &ShaderResourceView);

	if (FAILED(hr))
	{
		std::cerr << "Failed to create shader resource view for fullscreen render Error: " << HRESULT_CODE(hr);
	}

	D3D11_RENDER_TARGET_VIEW_DESC RenderToQuadRTVDESC;
	RenderToQuadRTVDESC.Format = FullScreenTextureDESC.Format;
	RenderToQuadRTVDESC.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RenderToQuadRTVDESC.Texture2D.MipSlice = 0;

	hr = Device->CreateRenderTargetView(FullScreenTexture, &RenderToQuadRTVDESC, &RenderTargetView);

	if (FAILED(hr))
	{
		std::cerr << "Failed to create render target view for FullScreen RenderTarget Error code: " << HRESULT_CODE(hr);
	}

	FullScreenTexture->Release();

	D3D11_TEXTURE2D_DESC DepthStencilDESC;
	ZeroMemory(&DepthStencilDESC, sizeof(DepthStencilDESC));

	DepthStencilDESC.Width = InWidth;
	DepthStencilDESC.Height = InHeight;
	DepthStencilDESC.MipLevels = 1;
	DepthStencilDESC.ArraySize = 1;
	DepthStencilDESC.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthStencilDESC.SampleDesc.Count = 1;
	DepthStencilDESC.SampleDesc.Quality = 0;
	DepthStencilDESC.Usage = D3D11_USAGE_DEFAULT;
	DepthStencilDESC.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	ID3D11Texture2D* DepthStencilBuffer = nullptr;
	hr = Device->CreateTexture2D(&DepthStencilDESC, nullptr, &DepthStencilBuffer);

	if (FAILED(hr))
	{
		std::cerr << "Failed to create Texture2D for DepthStencilBuffer for FullScreen RenderTarget Error code: " << HRESULT_CODE(hr);
	}

	hr = Device->CreateDepthStencilView(DepthStencilBuffer, nullptr, &DepthStencilView);

	DepthStencilBuffer->Release();

	if (FAILED(hr))
	{
		std::cerr << "Failed to create DepthStencilBuffer for FullScreen RenderTarget Error code: " << HRESULT_CODE(hr);
	}
}

FullScreenRenderTarget::~FullScreenRenderTarget()
{
	//if (FullScreenTexture) FullScreenTexture->Release();

	if(RenderTargetView) RenderTargetView->Release();
	if(ShaderResourceView) ShaderResourceView->Release();
	if(DepthStencilView) DepthStencilView->Release();
	if (VertexShader) VertexShader->Release();
	if (PixelShader) PixelShader->Release();
	
	if (Layout) Layout->Release();
	if (VertexBuffer) VertexBuffer->Release();
	if (IndexBuffer) IndexBuffer->Release();

}

void FullScreenRenderTarget::SetOMRenderTarget()
{
	//if (!RenderTargetView)
	//{
	//	std::cerr << "Render Target view null????";
	//}

	float ClearColor[4] = { 0.5f, 0.5f, 0.5f, 1.0f }; // red,green,blue,alpha
	ImmediateContext->ClearRenderTargetView(RenderTargetView, ClearColor);
	ImmediateContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	ImmediateContext->OMSetRenderTargets(1, &RenderTargetView, DepthStencilView);
}

HRESULT FullScreenRenderTarget::InitIndexBuffer()
{
	HRESULT hr;
	// Create Quad index buffer
	WORD QuadIndices[] =
	{
		0, 3, 1,
		3, 2, 1,
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = QuadIndices;

	hr = Device->CreateBuffer(&bd, &InitData, &IndexBuffer);

	if (FAILED(hr))
		return hr;

	return hr;
}
HRESULT FullScreenRenderTarget::InitVertexBuffer()
{
	HRESULT hr;

	// Create vertex buffer for Full Screen Quad
	vertex_deferred QuadVerts[] =
	{
		{ XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 0.0f),  XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(vertex_deferred) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = QuadVerts;

	hr = Device->CreateBuffer(&bd, &InitData, &VertexBuffer);

	if (FAILED(hr))
		return hr;

	return hr;

}


HRESULT FullScreenRenderTarget::InitShaders(ID3DBlob* VS_Blob, ID3DBlob* PS_Blob)
{
	HRESULT hr;

	// Create the vertex shader for Screen Space FX
	hr = Device->CreateVertexShader(VS_Blob->GetBufferPointer(), VS_Blob->GetBufferSize(), nullptr, &VertexShader);

	if (FAILED(hr))
	{
		VS_Blob->Release();
		return hr;
	}

	// Create the pixel shader for Screen Space FX
	hr = Device->CreatePixelShader(PS_Blob->GetBufferPointer(), PS_Blob->GetBufferSize(), nullptr, &PixelShader);

	if (FAILED(hr))
	{
		PS_Blob->Release();
		return hr;
	}

	return hr;

}

HRESULT FullScreenRenderTarget::InitInputLayout(ID3DBlob* VS_Blob)
{
	HRESULT hr;
	// Define the input layout for Screen Space FX
	D3D11_INPUT_ELEMENT_DESC LayoutDESC[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }, //DXGI_FORMAT_R32G32B32_FLOAT
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	UINT NumElements = ARRAYSIZE(LayoutDESC);

	// Create the input layout for SSFX
	hr = Device->CreateInputLayout(LayoutDESC, NumElements, VS_Blob->GetBufferPointer(), VS_Blob->GetBufferSize(), &Layout);

	return hr;
}

HRESULT FullScreenRenderTarget::InitShadersAndInputLayout(ID3DBlob* VS_Blob, ID3DBlob* PS_Blob)
{
	//TODO: Put in some actual checking here
	HRESULT hr;
	hr = InitShaders(VS_Blob, PS_Blob);
	hr = InitInputLayout(VS_Blob);

	return hr;
}

void FullScreenRenderTarget::RenderFromTexture(ID3D11SamplerState* Sampler)
{
	ImmediateContext->IASetInputLayout(Layout);

	UINT stride = sizeof(vertex_deferred);
	UINT offset = 0;

	ImmediateContext->IASetVertexBuffers(0, 1, &VertexBuffer, &stride, &offset);
	ImmediateContext->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	ImmediateContext->VSSetShader(VertexShader, nullptr, 0);
	ImmediateContext->PSSetShader(PixelShader, nullptr, 0);

	//ImmediateContext->PSSetSamplers(0, 1, &Sampler);

	//convert RenderToQuadRTV to RenderedQuadForSSFX
	//ID3D11ShaderResourceView* tempShaderRV = DeferredRenderTarget->GetShaderResourceView();

	ImmediateContext->PSSetShaderResources(10, 1, &ShaderResourceView);

	ImmediateContext->DrawIndexed(6, 0, 0);
}

